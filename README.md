# Gitea integration for Rocket.Chat

## Aufgaben
- [ ] Alles alle 30 Minuten synchronisieren
  - [ ] Öffentliche Räume: Öffentliche Repositories und Organisationen (`Codeberg` und `Codeberg/Community`)
  - [ ] Private Kanäle: Private Repositories (e.g. `Codeberg/Discussion`)
  - [ ] Private Kanäle: Teams in Organisationen (e.g. `Codeberg/@Owners`)
  - [ ] Moderatoren: Collaborators mit Schreibzugriff
  - [ ] Administratoren: Gitea-Admins
  - [ ] Schreibgeschützt ohne Moderatoren: archivierte Repos (bzw. Kanal archivieren)
  - [ ] Hook zum Umbenennen?
- [ ] Gitea-Integration
  - [ ] Neuer Tab mit iFrame zu korrektem Kanal
  - [ ] Tab über Einstellungen aktivierbar/deaktivierbar machen
  - [ ] Auch für Organisationen
  - [ ] Bei iFrame Menüknopf verstecken und durch "in neuem Tab öffnen"-Knopf ersetzen
  - [ ] Link für DM bei Nutzern & Link zu Raum bei Organisationen
  - [ ] Repos, Benutzer und Organisationen umbenennen (?)
  - [ ] Irgendwie Notifications in GitLab anzeigen & E-Mail-Notifications standardmäßig aktivieren
- [ ] Einzelne Repositories bei Events aktualisieren
  - [ ] Chat aktivieren/deaktivieren
  - [ ] Repo erstellen/löschen
  - [ ] Member verwalten (auch in Organisationen)
- [ ] Infrastruktur für Codeberg einrichten
- [ ] Englische Dokumentation schreiben damit das Projekt auch sinnvoll von allen genutzt werden kann

## Plan zur Synchronisierung
1. Mit `/api/v1/admin/orgs` Organisationen auflisten
2. Sicherstellen dass die Organisationskanäle existieren und privat/öffentlich sind
3. Sicherstellen dass die Administratoren der entsprechenden Organisationskanäle nur die Leute sind die mindestens Schreibzugriff auf die komplette Organisation haben
4. Mit `/api/v1/orgs/{org}/teams` Teams der Organisationen auflisten
5. Sicherstellen dass die entsprechenden Team-Kanäle existieren und privat sind
6. Mit `/api/v1/teams/{team}/members` Team-Mitglieder auflisten
7. Sicherstellen dass die Team-Mitglieder die einzigen Nutzer im entsprechenden Team-Kanal sind
8. Sicherstellen dass die Moderatoren der Team-Kanäle nur die Leute sind die mindestens Schreibzugriff auf die komplette Organisation haben
9. Mit `/api/v1/orgs/{org}/repos` Repositories auflisten
10. Sicherstellen dass die entsprechenden Repo-Kanäle existieren und privat/öffentlich sind
11. Sicherstellen dass die Nutzer in den privaten Kanälen nur die Teammitglieder und Collaborators sind, die Zugriff auf dieses Repo haben
12. Sicherstellen dass die Nutzer in den öffentlichen Kanälen mindestens die Teammitglieder oder Collaborators sind, die Zugriff auf dieses Repo haben (keine Nutzer entfernen)
13. Sicherstellen dass die Repo-Kanäle archivierter Repositories schreibgeschützt sind und keine Moderatoren haben
14. Sicherstellen dass die Moderatoren nicht archivierter Repositories nur die Teammitglieder oder Collaborators sind die Schreibzugriff auf dieses Repo haben
15. Mit `/api/v1/admin/users` Nutzer auflisten
16. Mit `/api/v1/users/{user}/repos` Repos der User auflisten
17. Sicherstellen dass die entsprechenden Repo-Kanäle existieren und privat/öffentlich sind
18. Sicherstellen dass die Nutzer in den privaten Kanälen nur die Collaborators sind, die Zugriff auf dieses Repo haben
19. Sicherstellen dass die Nutzer in den öffentlichen Kanälen mindestens die Collaborators sind, die Zugriff auf dieses Repo haben (keine Nutzer entfernen)
20. Sicherstellen dass die Repo-Kanäle archivierter Repositories schreibgeschützt sind und keine Moderatoren haben
21. Sicherstellen dass die Moderatoren nicht archivierter Repositories nur die Collaborators sind die Schreibzugriff auf dieses Repo haben
22. Alle Rocket.Chat-Kanäle auflisten
23. Alle Kanäle, die nicht bisher überprüft wurden, löschen.

Jeweils überprüfen ob Owner gesondert behandelt werden müssen?
Jeweils in den Einstellungen von Organisationen & Repositories den Kanal deaktivieren bzw. privat schalten

## Idee wie das ganze in Go aussehen könnte

```go
gitea.Connect("https://codeberg.org")
gitea.Authenticate("...")
rocketchat.Connect("https://chat.codeberg.org")
rocketchat.Authenticate("...")
channels := map[string]struct{}{}
for _, org := range gitea.Admin.Orgs() {
	channel := rocketchat.Channel(org.Username)
	if channel == nil {
		channel = rocketchat.NewChannel(org.Username)
	}
	channels[org.Username] = struct{}{}
	// TODO
	for _, team := range org.Teams() {
		// TODO
		members := team.Members()
		// TODO
	}
	for _, repo := range org.Repos() {
		// TODO
	}
}
for _, user := range gitea.Admin.Users() {
	for _, repo := range user.Repos() {
		// TODO
	}
}
for _, channel := range rocketchat.Channels() {
	if _, ok := channels[channel.Name]; !ok {
		channel.Delete()
	}
}
```

Die entsprechenden Client-Libraries gibt es noch nicht, evtl. kann man diese aus der Swagger-Definition generieren? Bei Gitea sollte das kein Problem sein, Rocket.Chat scheint da komplexer zu sein.
